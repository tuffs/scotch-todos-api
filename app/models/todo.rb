class Todo < ApplicationRecord
  # model description
  has_many :items, dependent: :destroy

  # validations
  validates_presence_of :title, :created_by
end
